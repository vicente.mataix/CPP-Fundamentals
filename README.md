# C++ Fundamentals

### Try it online with [Binder](http://mybinder.org/)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fvicente.mataix%2FCPP-Fundamentals/HEAD?filepath=Ch00-TableOfContents.ipynb)

- Jupyter Notebooks for Computer Science Foundational Concepts (CS1) using C++
- Some of the contents are based on the open source textbook: Think C++ by Allen B. Downey
    - others are based on [https://en.cppreference.com/](https://en.cppreference.com/) and [http://cplusplus.com/](http://cplusplus.com/)
- PDF of the textbook can be found at [http://www.greenteapress.com/thinkcpp/thinkCScpp.pdf](http://www.greenteapress.com/thinkcpp/thinkCScpp.pdf)

## PDF Format

- PDF format of each chapter notebook can be found in [pdfs](https://github.com/rambasnet/CPPFundamentals-Notebooks/tree/master/pdfs) folder

## System Requirements

- Linux/MacOS/WSL on Windows
- Jupyter Notebook - learning evinornment
- xeus-cling kernel - to run C++ code in Jupyter notebook
- git client - to use version control
- G++ compiler - to compile and run sample codes, solve exercises and labs
- VS Code - Editor to write C++ programs

### Note: xeus-cling doesn't support Windows

    - As of September 2020

## Development Environment Setup

- Detail step-by-step instructions (with screenshots) can be found here: [https://github.com/rambasnet/DevEnvSetup](https://github.com/rambasnet/DevEnvSetup)

## Install required tools

- If you went through Development Environment Setup step, you can ignore this section
- Note: these libraries and tools need to be installed just once, if you've Jupyter Notebook with C++ Kernel, you can safely ignore this section

- Git client is already available on Mac and Linux
- Install Miniconda: [https://conda.io/miniconda.html](https://conda.io/miniconda.html)
- Open a terminal/shell and run the following commands
- Create a virual environment to keep C++ specific installtions seperate from base installation

    ```bash
        conda update conda
        conda create -n cpp python=3.7
        conda activate cpp
        conda install notebook
        conda install -c conda-forge xeus-cling
        conda install -c conda-forge jupyter_contrib_nbextensions
        conda install -c conda-forge jupyter_nbextensions_configurator
        jupyter nbextensions_configurator enable --user
    ```

## Run or use Jupyter Notebooks

- Clone the repository locally once the tools are installed
- Open a terminal and `cd` into this cloned repo and run jupyter notebook

    ```bash
        cd <CPP Fundamentals repo folder>
        conda activate cpp # activate virtualenv cpp if created
        jupyter notebook
    ```

- Enter ctrl+c to stop jupyter notebook from the terminal where its running from
- $ conda deactivate # to deactivate the virtual env and go back to base installation

## Demo programs and sample solutions

- Jupyter Notebook doesn't run complete C++ program with main()
- Complete demo programs and example solutions are provided in demo_programs folder
- These programs need to be compiled using any C++ compiler (with C++11 or newer standandard)

## Compiling and running C++ programs

### Compiling with g++ compiler from terminal

- Open a terminal and run the following commands
- `cd` into a folder with a demo program; compile and run the program

    ```bash
       cd demo_programs/Ch...
       g++ -g -Wall -std=c++17 -o programName inputFile.cpp
       ./programName
    ```

### Compiling with Make program

- See GNU Make: [https://www.gnu.org/software/make/](https://www.gnu.org/software/make/)
- Create a Makefile in the project folder; see makefile_demos folder for examples
- Once the Makefile is created; use the following commands from terminal
- `cd` into the project folder with the Makefile; compile and run the program using make command

    ```bash
        make
        make run
        make clean
    ```

## Compiling and running C++ programs with VS Code

- Install C/C++ extensions for VS Code
- Open integreted terminal: View -> Terminal and follow the commands above
- You can also click run button on the top right corner for some programs, but not recommended for many programs that are interactive
